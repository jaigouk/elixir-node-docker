FROM elixir:1.5.1

RUN apt-get update && apt-get upgrade -y && apt-get install locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# update and install software
RUN apt-get install -y curl wget git make sudo

ENV PHOENIX_VERSION 1.3.0

# install the Phoenix Mix archive
RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new-$PHOENIX_VERSION.ez
RUN mix local.hex --force \
    && mix local.rebar --force

# node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - && sudo apt-get install -y nodejs \
    && rm -rf /var/cache/apt \
    && npm install -g yarn

# Install openssh
RUN apt-get install openssh-client -y

WORKDIR /code
